local PART={}
PART.ID = "coralbswitch2"
PART.Name = "2005 TARDIS Blue Switch 2"
PART.Model = "models/doctorwho1200/coral/blueswitch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/coral/plasticswitch.wav"

TARDIS:AddPart(PART)