local PART={}
PART.ID = "coralvalve"
PART.Name = "2005 TARDIS Valve"
PART.Model = "models/doctorwho1200/coral/valve.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

PART.Sound =  "doctorwho1200/coral/valve.wav" 

TARDIS:AddPart(PART)