-- The console

local PART={}
PART.ID = "coralconsole"
PART.Name = "The Console"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.Model = "models/doctorwho1200/coral/console.mdl"

TARDIS:AddPart(PART)