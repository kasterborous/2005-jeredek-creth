local PART={}
PART.ID = "coralgswitch"
PART.Name = "2005 TARDIS Green Switch"
PART.Model = "models/doctorwho1200/coral/greenswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/coral/plasticswitch.wav"

TARDIS:AddPart(PART)