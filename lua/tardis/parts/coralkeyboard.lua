local PART={}
PART.ID = "coralkeyboard"
PART.Name = "2005 TARDIS Keyboard"
PART.Model = "models/doctorwho1200/coral/keyboard.mdl"
PART.AutoSetup = true
PART.Collision = true

PART.Sound =  "doctorwho1200/coral/keyboard.wav"

TARDIS:AddPart(PART)