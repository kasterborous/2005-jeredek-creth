local PART={}
PART.ID = "coralcrank"
PART.Name = "2005 TARDIS Crank"
PART.Model = "models/doctorwho1200/coral/crank.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.8

PART.Sound =  "doctorwho1200/coral/crank.wav" 

TARDIS:AddPart(PART)