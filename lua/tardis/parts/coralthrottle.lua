local PART={}
PART.ID = "coralthrottle"
PART.Name = "2005 TARDIS Throttle"
PART.Model = "models/doctorwho1200/coral/throttle.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/coral/throttle.wav"

TARDIS:AddPart(PART)