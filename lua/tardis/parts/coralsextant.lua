local PART={}
PART.ID = "coralsextant"
PART.Name = "2005 TARDIS Sextant"
PART.Model = "models/doctorwho1200/coral/sextant.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/coral/sextant.wav"

TARDIS:AddPart(PART)