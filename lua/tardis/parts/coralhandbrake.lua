local PART={}
PART.ID = "coralhandbrake"
PART.Name = "2005 TARDIS Handbrake"
PART.Model = "models/doctorwho1200/coral/handbrake.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.SoundOn = "doctorwho1200/coral/handbrakeon.wav"
PART.SoundOff = "doctorwho1200/coral/handbrakeoff.wav"

TARDIS:AddPart(PART)