local PART={}
PART.ID = "corallever3"
PART.Name = "2005 TARDIS Lever 3"
PART.Model = "models/doctorwho1200/coral/lever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.SoundOff = "doctorwho1200/coral/lever3off.wav"
PART.SoundOn = "doctorwho1200/coral/lever3on.wav"

TARDIS:AddPart(PART)