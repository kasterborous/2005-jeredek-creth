local PART={}
PART.ID = "coraltoggles"
PART.Name = "2005 TARDIS Toggles"
PART.Model = "models/doctorwho1200/coral/toggles.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.8
PART.SoundOn = "doctorwho1200/coral/toggleson.wav"
PART.SoundOff = "doctorwho1200/coral/togglesoff.wav"

TARDIS:AddPart(PART)