local PART={}
PART.ID = "corallever"
PART.Name = "2005 TARDIS Lever"
PART.Model = "models/doctorwho1200/coral/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/coral/lever.wav"

TARDIS:AddPart(PART)