local PART={}
PART.ID = "coralbell"
PART.Name = "2005 TARDIS Bell"
PART.Model = "models/doctorwho1200/coral/bell.mdl"
PART.AutoSetup = true
PART.Collision = true

PART.Sound =  "doctorwho1200/coral/bell.wav" 

TARDIS:AddPart(PART)