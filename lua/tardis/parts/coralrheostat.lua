local PART={}
PART.ID = "coralrheostat"
PART.Name = "2005 TARDIS Rheostat"
PART.Model = "models/doctorwho1200/coral/rheostat.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.55

PART.Sound =  "doctorwho1200/coral/rheostat.wav" 

TARDIS:AddPart(PART)