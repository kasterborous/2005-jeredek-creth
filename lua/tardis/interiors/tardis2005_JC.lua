-- 2005 TARDIS

local T={
	Base="base",
	Name="2005 JC adaptation",
	ID="tardis2005_JC",
	IsVersionOf = "tardis2005",
}

T.Interior={

	Model="models/doctorwho1200/coral/interior.mdl",

	Portal={
		pos=Vector(317.36,0,49.02),
		ang=Angle(0,180,0),
		width=80,
		height=100
	},

	Fallback={
		pos=Vector(296,0,10),
		ang=Angle(0,180,0),
	},

	ExitDistance=400,


	Light={
		color=Color(10,150,70),
		pos = Vector(0, 0, 138),
		brightness = 3,
		warncolor = Color(255,0,0),
	},
	Lights={
		seclight={
			color=Color(200,150,0),
			pos=Vector(180,0,120),
			brightness=1,
			warncolor = Color(255,0,0),
		},
		seclight2={
			color=Color(200,150,0),
			pos=Vector(-130,0,160),
			brightness=1,
			warncolor = Color(255,0,0),
		},
	},
	LightOverride = {
		basebrightness = 0.01,
		nopowerbrightness = 0.05
	},
	Sounds={
		Teleport={
			demat="jeredek/coral/demat_int.wav",
			demat_damaged = "jeredek/coral/demat_int_stut.wav",
			mat="jeredek/coral/mat_int.wav",
			mat_damaged = "poogie/coral/mat_stut.wav",
			demat_fail = "poogie/coral/demat_fail.wav",
		},
		Power = {
            On = "poogie/coral/poweron.wav",
        },
		Lock="jeredek/coral/lock.wav",
		Door={
			enabled=true,
			open="doctorwho1200/coral/door_open.wav",
			close="doctorwho1200/coral/door_close.wav"
		},
		FlightLoop="doctorwho1200/coral/flight_loop.wav",
		FlightLoopDamaged = "jeredek/coral/flight_loop_stut.wav",
	},


	IdleSound={
		{
			path="doctorwho1200/coral/interior.wav",
			volume=1
		}
	},

	Screens={
		{
			pos=Vector(-23,23,96),
			ang=Angle(180,60,-102),
			width=269,
			height=220
		}
	},

	Seats={
		{
			pos=Vector(-110,0,60),
			ang=Angle(0,-90,0)
		},
		{
			pos=Vector(-110,20,60),
			ang=Angle(0,-90,0)
		},
		{
			pos=Vector(-110,-20,60),
			ang=Angle(0,-90,0)
		},
	},

	Parts={
		coralconsole=true,
		coralaudio=true,
		coralball=true,
		coralball2=true,
		coralbell=true,
		coralbswitch=true,
		coralbswitch2=true,
		coralbutton=true,
		coralchair=true,
		coralcpipes=true,
		coralcrank=true,
		coralcrank2=true,
		coraldoorframe=true,
		coralflight=true,
		coralgswitch=true,
		coralgswitch2=true,
		coralhandbrake=true,
		coralhandle=true,
		coralhandle2=true,
		coralinterior2=true,
		coralkeyboard=true,
		coralkeyboard2=true,
		corallever={
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
		},
		corallever2=true,
		corallever3=true,
		corallights=true,
		coralmonitor=true,
		coralphone=true,
		coralpipes=true,
		coralpump=true,
		coralpump2=true,
		coralpump3=true,
		coralpump4=true,
		coralreg=true,
		coralreg2=true,
		coralrheostat=true,
		coralrs=true,
		coralsextant=true,
		coralstuff=true,
		coralstuff2=true,
		coralstuff3=true,
		coralstuff4=true,
		coralstuff5=true,
		coralstuff6=true,
		coralsw=true,
		coralsw2={
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
		},
		coralswitch=true,
		coralswitch2=true,
		coralthrottle=true,
		coraltoggles=true,
		coraltoggles2=true,
		coralvalve=true,
		coralvalve2=true,
		coralvalve3=true,
		coralvalve4=true,
		coralvalve5=true,
		coralvt=true,
		coralwalls=true,
		coralwheel=true,
		coralwheel2={
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
		},
		door={
			model="models/doctorwho1200/coral/doors.mdl",posoffset=Vector(0,0,-43.28),angoffset=Angle(0,180,0)
		},
	},
	Controls = {
		coralconsole = "thirdperson_careful",
		coralbswitch2 = "door",
		coralgswitch = "hads",
		coralkeyboard2 = "doorlock",
		corallever = "physlock",
		corallever2 = "vortex_flight",
		corallever3 = "float",
		coralsextant = "repair",
		coraltoggles = "cloak",
		coralswitch = "power",
		coralbutton = "fastreturn",
		coralthrottle = "teleport_double",
		coralhandbrake = "handbrake",
		coralflight = "flight",
		coralvalve5 = "toggle_screens",
		coralkeyboard = "coordinates",
		coralphone = "coral_light_switch",
	},

}
T.Exterior={
	Model="models/doctorwho1200/coral/exterior.mdl",
	WinterSkins = { 3 },

	Mass=2000,

	Portal={
		pos=Vector(25.63,0,48),
		ang=Angle(0,0,0),
		width=50,
		height=92
	},

	Fallback={
		pos=Vector(35,0,5),
		ang=Angle(0,0,0)
	},

	Light={
		enabled=true,
		pos=Vector(0,0,120),
		color=Color(255,240,160),
		warncolor = Color(255,150,20),
	},

	Sounds={
		Teleport={
			demat="doctorwho1200/coral/demat.wav",
			demat_fail="poogie/coral/demat_fail.wav",
			demat_damaged = "jeredek/coral/demat_stut.wav",
			mat="doctorwho1200/coral/mat.wav",
			mat_damaged = "poogie/coral/mat_stut.wav",
		},
		Lock="doctorwho1200/coral/lock.wav",
		Door={
			enabled=true,
			open="doctorwho1200/coral/door_open.wav",
			close="doctorwho1200/coral/door_close.wav"
		},
		FlightLoop="doctorwho1200/coral/flight_loop.wav",
		FlightLoopDamaged = "jeredek/coral/flight_loop_stut.wav",
	},

	Parts={
		door={
			model="models/doctorwho1200/coral/doorsext.mdl",posoffset=Vector(0,0,-43.28),angoffset=Angle(0,0,0)
		},
		vortex={
			model="models/doctorwho1200/coral/2005timevortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,0,0),
			scale=10
		}
	},
	Teleport = {
        SequenceSpeed = 0.85,
        SequenceSpeedWarning = 0.6,
    }
}
T.Exterior.TextureSets = {
	old_textures = {
		prefix = "models/doctorwho1200/coral_old/",
		{"self", 0, "2005"},
		{"self", 1, "policeboxsign"},
		{"self", 2, "windows"},
		{"self", 3, "2005l"},
		{"self", 4, "2005spray"},
		{"self", 5, "doorsign"},
		{"self", 6, "windows2"},
		{"self", 7, "windows3"},
		{"self", 8, "2005snow"},
		{"self", 9, "policeboxsignbadwolf"},
		{"self", 10, "doorsignbadwolf"},
		{"door", 0, "2005"},
		{"door", 1, "windows"},
		{"door", 2, "doorsign"},
		{"door", 3, "policeboxsign"},
		{"door", 4, "windows2"},
		{"door", 5, "windows3"},
		{"door", 6, "2005snow"},
		{"door", 7, "policeboxsignbadwolf"},
		{"door", 8, "doorsignbadwolf"},
		{"vortex", 0, "2005blue"},
		{"vortex", 1, "2005red"},
	},
}
T.Interior.TextureSets = {
	old_textures = {
		prefix = "models/doctorwho1200/coral_old/",
		{"self", 0, "walls"},
		{"self", 1, "columns"},
		{"self", 2, "hatstand"},
		{"self", 3, "rails"},
		{"coralbswitch2", 0, "plasticswitches"},
		{"coralbell", 0, "stuff4"},
		{"coralflight", 0, "stuff"},
		{"coralvalve2", 0, "stuff5"},
		{"coralvalve3", 0, "stuff"},
		{"coralsw2", 0, "stuff"},
		{"coralsw2", 1, "details2"},
		{"coralcrank2", 0, "stuff4"},
		{"coralvalve5", 0, "stuff3"},
		{"coralwheel", 0, "wheel"},
		{"coralwheel2", 0, "wheel"},
		{"coralcpipes", 0, "pipes"},
		{"coralcpipes", 1, "stuff4"},
		{"coralcpipes", 2, "rustymetal"},
		{"coralvalve4", 0, "stuff"},
		{"coralvalve", 0, "stuff4"},
		{"coralpump", 0, "pipes"},
		{"coralpump", 1, "stuff3m"},
		{"coralstuff6", 0, "pipes"},
		{"coralstuff6", 1, "stuff6"},
		{"coralstuff6", 2, "details"},
		{"coralstuff6", 3, "rustymetal"},
		{"coralstuff6", 4, "wires"},
		{"coralstuff6", 5, "glass"},
		{"corallever3", 0, "stuff5"},
		{"corallever3", 1, "stuff5m"},
		{"coralpump2", 0, "stuff4"},
		{"coralpump2", 1, "pipes"},
		{"coralhandle2", 0, "stuff2"},
		{"coralhandbrake", 0, "stuff3"},
		{"corallights", 0, "consolecentre"},
		{"corallights", 1, "bubbles"},
		{"corallights", 2, "plastic"},
		{"corallights", 3, "lowerlights"},
		{"coralpump4", 0, "stuff6"},
		{"coralpump4", 1, "details"},
		{"coralpump4", 2, "pipes"},
		{"coralhandle", 0, "stuff3"},
		{"coralaudio", 0, "stuff2"},
		{"coralaudio", 1, "details3"},
		{"coralsw", 0, "stuff"},
		{"coralsw", 1, "details2"},
		{"coralinterior2", 0, "details"},
		{"coralinterior2", 1, "grate"},
		{"coralinterior2", 2, "consolegrate"},
		{"coralinterior2", 3, "doorl"},
		{"coralinterior2", 4, "door"},
		{"coralinterior2", 5, "rustymetal"},
		{"coralinterior2", 6, "floor"},
		{"coralinterior2", 7, "woodfloor"},
		{"coralinterior2", 8, "glass"},
		{"coralinterior2", 9, "rotor"},
		{"coralinterior2", 10, "greenglass"},
		{"coralinterior2", 11, "magnifier"},
		{"coralinterior2", 12, "blueglass"},
		{"coralstuff5", 0, "pipes"},
		{"coralstuff5", 1, "wires"},
		{"coralstuff5", 2, "stuff5m"},
		{"coralstuff5", 3, "stuff5"},
		{"coralstuff5", 4, "rustymetal"},
		{"coralstuff5", 5, "details"},
		{"coralpump3", 0, "stuff5"},
		{"coralpump3", 1, "pipes"},
		{"coralphone", 0, "stuff5"},
		{"coralphone", 1, "details"},
		{"coralchair", 0, "pipes"},
		{"coralchair", 1, "chair"},
		{"coraltoggles", 0, "details2"},
		{"coraltoggles", 1, "stuff4"},
		{"coralcrank", 0, "stuff"},
		{"coralstuff2", 0, "rustymetal"},
		{"coralstuff2", 1, "pipes"},
		{"coralstuff2", 2, "wires"},
		{"coralstuff2", 3, "stuff2m"},
		{"coralstuff2", 4, "stuff2"},
		{"coralstuff2", 5, "details3"},
		{"coralstuff2", 6, "glass"},
		{"coralbswitch", 0, "plasticswitches"},
		{"coralreg2", 0, "stuff5"},
		{"coralreg2", 1, "details"},
		{"corallever", 0, "stuff"},
		{"coralstuff4", 0, "pipes"},
		{"coralstuff4", 1, "stuff4m"},
		{"coralstuff4", 2, "rustymetal"},
		{"coralstuff4", 3, "stuff4"},
		{"coralstuff4", 4, "details"},
		{"coralstuff4", 5, "details2"},
		{"coraldoorframe", 0, "2005"},
		{"coraldoorframe", 1, "door"},
		{"coraldoorframe", 2, "policeboxsign"},
		{"coraldoorframe", 3, "doorl"},
		{"coraldoorframe", 4, "doorsign"},
		{"coraldoorframe", 5, "windows"},
		{"coraldoorframe", 6, "windows2"},
		{"coraldoorframe", 7, "windows3"},
		{"coraldoorframe", 8, "2005snow"},
		{"coraldoorframe", 9, "policeboxsignbadwolf"},
		{"coraldoorframe", 10, "doorsignbadwolf"},
		{"coralreg", 0, "stuff5"},
		{"coralreg", 1, "details"},
		{"coralkeyboard", 0, "stuff4"},
		{"coralkeyboard", 1, "details"},
		{"coralball", 0, "details"},
		{"coralvt", 0, "details"},
		{"coralvt", 1, "stuff5"},
		{"coralvt", 2, "stuff5m"},
		{"coralgswitch2", 0, "plasticswitches"},
		{"coralgswitch", 0, "plasticswitches"},
		{"coralstuff", 0, "stuffm"},
		{"coralstuff", 1, "stuff"},
		{"coralstuff", 2, "wires"},
		{"coralstuff", 3, "pipes"},
		{"coralstuff", 4, "details"},
		{"coralstuff", 5, "details2"},
		{"coralswitch2", 0, "stuff6"},
		{"coralswitch2", 1, "details"},
		{"coralswitch", 0, "stuff"},
		{"coralswitch", 1, "details2"},
		{"door", 0, "door"},
		{"door", 1, "doorl"},
		{"door", 2, "2005"},
		{"door", 3, "windows"},
		{"door", 4, "doorsign"},
		{"door", 5, "lock"},
		{"door", 6, "phone"},
		{"door", 7, "policeboxsign"},
		{"door", 8, "windows2"},
		{"door", 9, "windows3"},
		{"door", 10, "2005snow"},
		{"door", 11, "policeboxsignbadwolf"},
		{"door", 12, "doorsignbadwolf"},
		{"coralrs", 0, "stuff5"},
		{"coralbutton", 0, "stuff"},
		{"coralbutton", 1, "details2"},
		{"coralstuff3", 0, "pipes"},
		{"coralstuff3", 1, "stuff3"},
		{"coralstuff3", 2, "rustymetal"},
		{"coralstuff3", 3, "stuff3m"},
		{"coralstuff3", 4, "wires"},
		{"coralstuff3", 5, "details"},
		{"coraltoggles2", 0, "stuff5"},
		{"coraltoggles2", 1, "stuff5m"},
		{"coralrheostat", 0, "details"},
		{"coralrheostat", 1, "stuff6"},
		{"coralrheostat", 2, "pipes"},
		{"coralball2", 0, "details"},
		{"coralsextant", 0, "magnifier"},
		{"coralsextant", 1, "stuff2m"},
		{"coralkeyboard2", 0, "details3"},
		{"coralpipes", 0, "pipes"},
		{"coralmonitor", 0, "monitor"},
		{"coralmonitor", 1, "sticker"},
		{"coralmonitor", 2, "screen"},
		{"coralmonitor", 3, "monitorm"},
		{"coralwalls", 0, "walls"},
		{"coralwalls", 1, "door"},
		{"coralwalls", 2, "roundels"},
		{"coralwalls", 3, "ceiling"},
		{"coralthrottle", 0, "stuff3"},
		{"coralthrottle", 1, "details2"},
		{"coralthrottle", 2, "stuff3m"},
		{"coralthrottle", 3, "glass"},
		{"corallever2", 0, "stuff4"},
		{"coralconsole", 0, "console"},
		{"coralconsole", 1, "crackle"},
		{"coralconsole", 2, "pipes"},
		{"coralconsole", 3, "rustymetal"},
	},
}
T.CustomHooks = {
	demat_shake_or_whatever = {
		exthooks = {["DematStart"] = true,},
		inthooks = {},
		func = function(ext, int)
			if SERVER then
				util.ScreenShake(int:GetPos(),5,100,20,700)
			end
		end,
	},
	failed_demat_shake = {
		exthooks = {["HandleNoDemat"] = true,},
		inthooks = {},
		func = function(ext, int)
			if SERVER then
				util.ScreenShake(int:GetPos(),5,100,5,700)
			end
		end,
	},
	initial_textures_or_whatever = {
		exthooks = {["Initialize"] = true,},
		inthooks = {["Initialize"] = true,},
		func = function(ext, int)
			ext:ApplyTextureSet("old_textures")
			if IsValid(int) then
				int:ApplyTextureSet("old_textures")
			end
		end,
	},
	handbrake_initial_value_i_think = {
		exthooks = {["PostInitialize"] = true,},
		inthooks = {},
		func = function(ext, int)
			if SERVER then
				ext:SetData("handbrake",true, true)
			end
		end,
	},
}

TARDIS:AddInterior(T)

