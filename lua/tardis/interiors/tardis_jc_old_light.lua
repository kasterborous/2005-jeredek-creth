

local T={
	Base="tardis2005_JC",
	Name="2005 JC adaptation",
	ID="tardis2005_JC_Green",
	IsVersionOf = "tardis2005",
}

T.Interior={

    Light={
        color=Color(10,150,70),
        pos = Vector(0, 0, 138),
        brightness = 6,
        warncolor = Color(255,0,0),
    },
    Lights={
        seclight={
            color=Color(200,150,100),
            pos=Vector(255.085, 1.2, 50.918),
            brightness=0.8,
            warncolor = Color(255,70,70),
        },
        seclight2={
            color=Color(200,150,0),
            pos=Vector(-130,0,160),
            brightness=0,
            warncolor = Color(255,0,0),
        },
    },
}
TARDIS:AddInterior(T)