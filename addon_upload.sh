#! /bin/bash

filename=tardis_extension_2005_jc.gma
[[ -z $* ]] && echo "Changelog not specified! Aborting." && exit 1
[[ ! -f ./$filename ]] && echo "File $filename does not exist! Aborting." && exit 2

gmpublish update -id 1975150399 -addon $filename -changes "$*"
[[ $? = 0 ]] && rm $filename
